package plus.life.ltm.life;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.ArrayList;

/**
 * Created by ASUS on 18/07/2017.
 */

public class SliderAdapter extends PagerAdapter {
    private ArrayList<Integer> listImages;
    private LayoutInflater inflater;
    private Context context;

    public SliderAdapter(Context context, ArrayList<Integer> listImages) {
        this.context = context;
        this.listImages = listImages;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View view = inflater.inflate(R.layout.slide, container, false);
        ImageView myImage = (ImageView) view.findViewById(R.id.image);
        myImage.setImageResource(listImages.get(position));
        container.addView(view, 0);
        return view;
    }

    @Override
    public int getCount() {
        return listImages.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }
}
